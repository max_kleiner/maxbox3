maXbox
======

****************************************************************
Release Notes maXbox 3.9.9.101 November 2014
****************************************************************

Add 30 Units, 1 Tutor, maXmap, OpenStreetView, MAPX, timers Function Menu/View/GEO Map View, DownloadFile, wgetX, sensors StreamUtils, IDL Syntax, OpenStreetMap, OpenMapX, LPT1, LazDOM ByteCode2, runByteCode, sensors, CGI-Powtils, IPUtils2, GPS_2

1006 unit uPSI_cyRunTimeResize; 1007 unit uPSI_jcontrolutils; 1008 unit uPSI_kcMapViewer; (+GEONames) 1009 unit uPSI_kcMapViewerDESynapse; 1010 unit uPSI_cparserutils; (+GIS_SysUtils) 1011 unit uPSI_LedNumber; 1012 unit uPSI_StStrL; 1013 unit uPSI_indGnouMeter; 1014 unit uPSI_Sensors; 1015 unit uPSI_pwnative_out; //CGI of powtils 1016 unit uPSI_HTMLUtil; 1017 unit uPSI_synwrap1; //httpsend 1018 unit StreamWrap1 1019 unit uPSI_pwmain; {beta} 1020 unit pwtypes 1021 uPSI_W32VersionInfo 1022 unit uPSI_IpAnim; 1023 unit uPSI_IpUtils; //iputils2 (TurboPower) 1024 unit uPSI_LrtPoTools; 1025 unit uPSI_Laz_DOM; 1026 unit uPSI_hhAvComp; 1027 unit uPSI_GPS2; 1028 unit uPSI_GPS; 1029 unit uPSI_GPSUDemo; // formtemplate TFDemo 1030 unit uPSI_NMEA; // GPS 1031 unit uPSI_ScreenThreeDLab; 1032 unit uPSI_Spin; //VCL
1033 unit uPSI_DynaZip; 1034 unit uPSI_clockExpert; 1035 unit debugLn

SHA1:    maXbox3.exe E951E93E67DE0E815D3F7441842994BAEA06F21A

CRC32: maXbox3.exe 78459738

****************************************************************
Release Notes maXbox 3.9.9.88 March 2014
****************************************************************
2 Tutorials 30 Units add, VCL constructors, controls+, unit list

786 uPSI_FileUtil;
787 uPSI_changefind;
788 uPSI_cmdIntf;
789 uPSI_fservice;
790 uPSI_Keyboard;
791 uPSI_VRMLParser,
792 uPSI_GLFileVRML,
793 uPSI_Octree;
794 uPSI_GLPolyhedron,
795 uPSI_GLCrossPlatform;
796 uPSI_GLParticles;
797 uPSI_GLNavigator;
798 uPSI_GLStarRecord;
799 uPSI_TGA;
800 uPSI_GLCanvas;
801 uPSI_GeometryBB;
802 uPSI_GeometryCoordinates;
803 uPSI_VectorGeometry;
804 uPSI_BumpMapping;
805 uPSI_GLTextureCombiners;
806 uPSI_GLVectorFileObjects;
807 uPSI_IMM;
808 uPSI_CategoryButtons;
809 uPSI_ButtonGroup;
810 uPSI_DbExcept;
811 uPSI_AxCtrls;
812 uPSI_GL_actorUnit1;
813 uPSI_StdVCL;
814 unit CurvesAndSurfaces;
815 uPSI_DataAwareMain;

SHA1: Win 3.9.9.88: 119533C0725A9B9B2919849759AA2F6298EBFF28